# Zorthgo.Toolbox.Extensions.String

This toolbox contains extension methods for the **string** type.

## **TryConvertTo:**

These extension methods allow the user to attempt to convert string values to a different type. If the conversion is successful, the method will return true and the ConvertedValue will be populated. Otherwise, if the conversion failed, the method will return false and ConvertedValue object will be null. The usages for this extension method is as follows:

**Using Generics**
```csharp

    // Value to be converted.
    var intString = "10";

    // Attempting to convert the string value above.
    if (!intString.TryConvertTo(out int convertedString))
        throw new Exception($"The value [{intString}] could not be converted to type [int].");

```

**Using Non-Generics**
```csharp
    // Value to be converted.
    var intString = "10";

    // Attempting to convert the string value above.
    if (!intString.TryConvertTo(typeof(int), out object convertedString))
        throw new Exception($"The value [{intString}] could not be converted to type [int].");
```

**Note:** When converting a string to boolean, the converter supports (True | False | 1 | 0) as valid boolean values.